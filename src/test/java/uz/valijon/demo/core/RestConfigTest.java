package uz.valijon.demo.core;

import static org.assertj.core.api.Assertions.assertThat;
import lombok.val;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.EnabledIf;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;
import uz.valijon.demo.config.RestConfig;

@EnabledIf(expression = "#{environment.acceptsProfiles('integration-test')}")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = RestConfig.class)
@TestPropertySource(properties = {
        "http.client.key.path=classpath:jClient.jks",
        "http.client.key.pwd=password",
})
class RestConfigTest {

    @Autowired
    private RestTemplate restTemplate;

    @Value("localhost")
    private String host;

    @Test
    void sslAuthenticationIsOk() {
        val url = String.format("https://%s", host);
        val response = restTemplate.exchange(url, HttpMethod.GET, HttpEntity.EMPTY, String.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}