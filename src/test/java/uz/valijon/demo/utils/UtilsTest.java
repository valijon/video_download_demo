package uz.valijon.demo.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.List;
import lombok.val;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import uz.valijon.demo.models.ContentServer;
import uz.valijon.demo.models.ContentStatus;

class UtilsTest {

    @Test
    void factorialSuccess() {
        val actual = Utils.factorial(6);
        val expected = 1 * 2 * 3 * 4 * 5 * 6;

        assertThat(actual).isEqualTo(expected);
    }

    @Nested
    public class TestDecideContentServer {

        final ContentServer server1 = mock(ContentServer.class);
        final ContentServer server2 = mock(ContentServer.class);
        final ContentServer server3 = mock(ContentServer.class);
        final String pathToResource = "/movies/testContent.mp4";

        @Test
        void decideServerSuccess() {
            Utils.contentServers.clear();
            Utils.contentServers.addAll(List.of(server1, server2, server3));

            val fastAndAvailable = ContentStatus.builder().isAvailable(true).latency(100).build();
            val slowAndAvailable = ContentStatus.builder().isAvailable(true).latency(1000).build();
            val unAvailable = ContentStatus.builder().isAvailable(false).build();

            when(server1.checkStatus(any(), any())).thenReturn(fastAndAvailable);
            when(server2.checkStatus(any(), any())).thenReturn(slowAndAvailable);
            when(server3.checkStatus(any(), any())).thenReturn(unAvailable);

            val actual = Utils.decideServer(pathToResource);
            val expected = server1;

            assertThat(actual).isEqualTo(expected);
        }

        @Test
        void decideServerSuccess2() {
            Utils.contentServers.clear();
            Utils.contentServers.addAll(List.of(server1, server2, server3));

            val fastAndAvailable = ContentStatus.builder().isAvailable(true).latency(100).build();
            val slowAndAvailable = ContentStatus.builder().isAvailable(true).latency(100).build();
            val unAvailable = ContentStatus.builder().isAvailable(false).build();

            when(server1.checkStatus(any(), any())).thenReturn(fastAndAvailable);
            when(server2.checkStatus(any(), any())).thenReturn(slowAndAvailable);
            when(server3.checkStatus(any(), any())).thenReturn(unAvailable);

            val actual = Utils.decideServer(pathToResource);
            val expected = unAvailable;

            assertThat(actual).isNotEqualTo(expected);
        }

        @Test
        void failNoServerFound() {
            Utils.contentServers.clear();
            assertThatThrownBy(() -> {
                Utils.decideServer(pathToResource);
            }).isInstanceOf(IllegalStateException.class)
              .hasMessage("No server is available");
        }

        @Test
        void failWhenAllServerAreDown() {
            Utils.contentServers.clear();
            Utils.contentServers.addAll(List.of(server1, server2, server3));

            val unAvailable1 = ContentStatus.builder().isAvailable(false).build();
            val unAvailable2 = ContentStatus.builder().isAvailable(false).build();
            val unAvailable3 = ContentStatus.builder().isAvailable(false).build();

            when(server1.checkStatus(any(), any())).thenReturn(unAvailable1);
            when(server2.checkStatus(any(), any())).thenReturn(unAvailable2);
            when(server3.checkStatus(any(), any())).thenReturn(unAvailable3);

            assertThatThrownBy(() -> {
                Utils.decideServer(pathToResource);
            }).isInstanceOf(IllegalStateException.class)
              .hasMessage("No server is available");
        }
    }
}