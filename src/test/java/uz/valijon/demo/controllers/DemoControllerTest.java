package uz.valijon.demo.controllers;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import uz.valijon.demo.core.VideoDownloadService;
import uz.valijon.demo.models.VideoMeta;

@WebMvcTest(controllers = DemoController.class)
class DemoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private VideoDownloadService videoDownloadService;

    @Test
    void downloadVideoSuccess() throws Exception {
        val body = VideoMeta.builder().build();
        val request = post("/download/video")
                .contentType(APPLICATION_JSON)
                .content(mapper.writeValueAsString(body));

        mockMvc.perform(request)
               .andExpect(status().isOk())
               .andExpect(jsonPath("$").doesNotExist());

        verify(videoDownloadService).asyncDownload(body);
        verifyNoMoreInteractions(videoDownloadService);
    }
}