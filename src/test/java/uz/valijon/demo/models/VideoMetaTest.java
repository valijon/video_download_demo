package uz.valijon.demo.models;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import java.util.List;
import java.util.UUID;
import lombok.val;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class VideoMetaTest {

    @Nested
    public class ValidationTest {

        @Test
        void validateSuccess() {

            val chunk1 = VideoChunkMeta.builder()
                                       .orderNumber(1)
                                       .size(1024L)
                                       .build();

            val chunk2 = VideoChunkMeta.builder()
                                       .orderNumber(2)
                                       .size(1024L)
                                       .build();

            val chunks = List.of(chunk1, chunk2);
            val meta = VideoMeta.builder()
                                .id(UUID.randomUUID())
                                .fileName("testFile")
                                .hash("some_hash")
                                .numberOfChunks(2)
                                .size(2048L)
                                .chunks(chunks)
                                .outputFilePath("outputFilePath")
                                .build();

            meta.validate();
        }

        @Test
        void failWhenIncorrectChunkSize() {

            val chunk1 = VideoChunkMeta.builder()
                                       .orderNumber(1)
                                       .size(500L)
                                       .build();

            val chunk2 = VideoChunkMeta.builder()
                                       .orderNumber(2)
                                       .size(1024L)
                                       .build();

            val chunks = List.of(chunk1, chunk2);
            val meta = VideoMeta.builder()
                                .id(UUID.randomUUID())
                                .fileName("testFile")
                                .hash("some_hash")
                                .numberOfChunks(2)
                                .size(2048L)
                                .chunks(chunks)
                                .outputFilePath("outputFilePath")
                                .build();

            assertThatThrownBy(() -> {
                meta.validate();
            }).isInstanceOf(IllegalArgumentException.class)
              .hasMessage("Chunks` sizes are incorrect!");
        }

        @Test
        void failWhenHashIsMissing() {
            val meta = VideoMeta.builder()
                                .id(UUID.randomUUID())
                                .fileName("testFile")
                                .build();

            assertThatThrownBy(() -> {
                meta.validate();
            }).isInstanceOf(IllegalArgumentException.class)
              .hasMessage("Incorrect hash for file!");

            meta.setHash("");
            assertThatThrownBy(() -> {
                meta.validate();
            }).isInstanceOf(IllegalArgumentException.class)
              .hasMessage("Incorrect hash for file!");
        }

        @Test
        void failWhenFileNameIsMissing() {
            val meta = VideoMeta.builder()
                                .id(UUID.randomUUID())
                                .hash("some_hash")
                                .build();

            assertThatThrownBy(() -> {
                meta.validate();
            }).isInstanceOf(IllegalArgumentException.class)
              .hasMessage("Incorrect file name!");

            meta.setFileName("");
            assertThatThrownBy(() -> {
                meta.validate();
            }).isInstanceOf(IllegalArgumentException.class)
              .hasMessage("Incorrect file name!");
        }

        @Test
        void failWhenWrongOrder() {

            val chunk1 = VideoChunkMeta.builder()
                                       .orderNumber(1)
                                       .size(1024L)
                                       .build();

            val chunk2 = VideoChunkMeta.builder()
                                       .orderNumber(3)
                                       .size(1024L)
                                       .build();

            val chunks = List.of(chunk1, chunk2);
            val meta = VideoMeta.builder()
                                .id(UUID.randomUUID())
                                .fileName("testFile")
                                .hash("some_hash")
                                .numberOfChunks(2)
                                .size(2048L)
                                .chunks(chunks)
                                .outputFilePath("outputFilePath")
                                .build();

            assertThatThrownBy(() -> {
                meta.validate();
            }).isInstanceOf(IllegalArgumentException.class)
              .hasMessage("Chunks contain wrong order number!");
        }

        @Test
        void failWhenMissingOutputFilePath() {

            val chunk1 = VideoChunkMeta.builder()
                                       .orderNumber(1)
                                       .size(1024L)
                                       .build();

            val chunk2 = VideoChunkMeta.builder()
                                       .orderNumber(2)
                                       .size(1024L)
                                       .build();

            val chunks = List.of(chunk1, chunk2);
            val meta = VideoMeta.builder()
                                .id(UUID.randomUUID())
                                .fileName("testFile")
                                .hash("some_hash")
                                .numberOfChunks(2)
                                .size(2048L)
                                .chunks(chunks)
                                .build();

            assertThatThrownBy(meta::validate)
                    .isInstanceOf(IllegalStateException.class)
                    .hasMessage("Output file path is not set!");
        }
    }

    @Nested
    public class CalcChunkPotionsTest {

        @Test
        void calculateChunkPositionsSuccess() {
            val chunk1 = VideoChunkMeta.builder()
                                       .orderNumber(1)
                                       .size(1024L)
                                       .build();

            val chunk2 = VideoChunkMeta.builder()
                                       .orderNumber(2)
                                       .size(1024L)
                                       .build();

            val chunk3 = VideoChunkMeta.builder()
                                       .orderNumber(3)
                                       .size(500L)
                                       .build();

            val chunk4 = VideoChunkMeta.builder()
                                       .orderNumber(3)
                                       .size(500L)
                                       .build();

            val chunks = List.of(chunk1, chunk2, chunk3, chunk4);
            val meta = VideoMeta.builder()
                                .numberOfChunks(3)
                                .chunks(chunks)
                                .build();

            meta.calculateChunkPositions();

            assertThat(chunk1.getStartPosition()).isEqualTo(0);
            assertThat(chunk2.getStartPosition()).isEqualTo(1024);
            assertThat(chunk3.getStartPosition()).isEqualTo(2048);
            assertThat(chunk4.getStartPosition()).isEqualTo(2548);
        }
    }
}