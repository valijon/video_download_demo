package uz.valijon.demo.models;

import static org.assertj.core.api.Assertions.assertThat;
import lombok.val;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.EnabledIf;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;
import uz.valijon.demo.config.RestConfig;

@EnabledIf(expression = "#{environment.acceptsProfiles('integration-test')}")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = RestConfig.class)
@TestPropertySource(properties = {
        "http.client.key.path=classpath:jClient.jks",
        "http.client.key.pwd=password"
})
class ContentServerTest {

    @Autowired
    private RestTemplate restTemplate;

    @Test
    void checkStatusSuccess() {
        val contentServer = ContentServer.builder()
                                         .host("localhost")
                                         .name("Local nginx")
                                         .build();

        val status = contentServer.checkStatus(restTemplate, "/movies/testContent.mp4");

        System.out.println("Latency " + status.getLatency());
        assertThat(status.isAvailable()).isTrue();
    }

    @Test
    void checkStatusWhenResourceIsMissing() {
        val contentServer = ContentServer.builder()
                                         .host("localhost")
                                         .name("Local nginx")
                                         .build();

        val status = contentServer.checkStatus(restTemplate, "/movies/missingFile.mp4");

        System.out.println("Latency " + status.getLatency());
        assertThat(status.isAvailable()).isFalse();
    }

    @Test
    void checkStatusWhenServerIsNotReachable() {
        val unreachableHost = "unreachable";
        val contentServer = ContentServer.builder()
                                         .host(unreachableHost)
                                         .name("Unreachable nginx")
                                         .build();

        val status = contentServer.checkStatus(restTemplate, "/movies/testContent.mp4");

        System.out.println("Latency " + status.getLatency());
        assertThat(status.isAvailable()).isFalse();
    }
}