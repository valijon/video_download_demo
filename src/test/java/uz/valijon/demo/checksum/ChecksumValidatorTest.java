package uz.valijon.demo.checksum;

import java.io.IOException;
import java.util.UUID;
import lombok.val;
import org.junit.jupiter.api.Test;
import uz.valijon.demo.models.VideoChunkMeta;
import uz.valijon.demo.util.FileProvider;

class ChecksumValidatorTest {

    private static final String EXPECTED_HASH_FOR_FLOWER_MP4 = "E7B29C2122D689EA20BCC7C1442A1D89";

    @Test
    void validateHash() throws IOException {
        val meta = VideoChunkMeta.builder()
                                 .id(UUID.randomUUID())
                                 .hash(EXPECTED_HASH_FOR_FLOWER_MP4)
                                 .build();

        val inputStream = FileProvider.getFlowerMp4();
        val validator = new ChecksumValidator(inputStream, meta);

        validator.getDigestInputStream().readAllBytes();
        validator.validateHash();
    }
}