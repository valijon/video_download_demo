package uz.valijon.demo.checksum;

import static org.assertj.core.api.Assertions.assertThat;
import java.io.IOException;
import lombok.val;
import org.junit.jupiter.api.Test;
import uz.valijon.demo.util.FileProvider;

class ChecksumCalculatorTest {

    private static final String EXPECTED_HASH_FOR_FLOWER_MP4 = "E7B29C2122D689EA20BCC7C1442A1D89";

    @Test
    void successCalcChecksum() throws IOException {
        val inputStream = FileProvider.getFlowerMp4();
        val checksumCalculator = new ChecksumCalculator();

        int read;
        byte[] buffer = new byte[1024];

        while ((read = inputStream.read(buffer)) > -1) {

            checksumCalculator.update(buffer, read);
        }
        inputStream.close();

        val actualHash = checksumCalculator.calcChecksum();

        assertThat(actualHash).isEqualTo(EXPECTED_HASH_FOR_FLOWER_MP4);
    }
}