package uz.valijon.demo.util;

import java.io.IOException;
import java.io.InputStream;
import org.springframework.core.io.ClassPathResource;

public class FileProvider {

    public static InputStream getFlowerMp4() throws IOException {
        return new ClassPathResource("flower.mp4").getInputStream();
    }

    public static InputStream bigTempFile() throws IOException {
        return new ClassPathResource("Avatar 2009.mp4").getInputStream();
    }
}
