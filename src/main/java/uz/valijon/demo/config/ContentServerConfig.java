package uz.valijon.demo.config;

import java.util.List;
import javax.annotation.PostConstruct;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import uz.valijon.demo.models.ContentServer;
import uz.valijon.demo.utils.Utils;

@Configuration
@ConfigurationProperties(prefix = "remote")
@Data
public class ContentServerConfig {

    private final List<ContentServer> servers;

    @Autowired
    private RestTemplate restTemplate;

    @PostConstruct
    public void initUtils() {
        Utils.init(restTemplate, this);
    }
}