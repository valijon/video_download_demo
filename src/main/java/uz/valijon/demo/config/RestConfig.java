package uz.valijon.demo.config;

import java.security.cert.X509Certificate;
import lombok.SneakyThrows;
import lombok.val;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestConfig {

    @Autowired
    private ResourceLoader resourceLoader;

    @Value("${http.client.key.path}")
    private String keyPath;
    @Value("${http.client.key.pwd}")
    private String keyPwd;

    @Bean
    public RestTemplate restTemplate() {
        val restTemplate = new RestTemplate();
        setSslContext(restTemplate);
        return restTemplate;
    }

    @SneakyThrows
    private void setSslContext(final RestTemplate restTemplate) {
        val url = resourceLoader.getResource(keyPath).getURL();
        val pwd = keyPwd.toCharArray();
        TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
        val sslContext = SSLContexts.custom()
                                    .loadTrustMaterial(null, acceptingTrustStrategy)
                                    .loadKeyMaterial(url, pwd, pwd)
                                    .build();

        val httpClient
                = HttpClients.custom()
                             .setSSLHostnameVerifier(new NoopHostnameVerifier())
                             .setSSLContext(sslContext)
                             .setMaxConnPerRoute(100)
                             .setMaxConnTotal(100)
                             .build();
        val requestFactory
                = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);

        restTemplate.setRequestFactory(requestFactory);
    }
}