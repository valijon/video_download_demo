package uz.valijon.demo.core;

import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Service;
import uz.valijon.demo.exception.FileSizeExceedsMetaInfoException;
import uz.valijon.demo.models.VideoChunkMeta;

@Service
@RequiredArgsConstructor
@Slf4j
public class VideoChunkWriter {

    public void writeContent(final InputStream inputStream,
                             final VideoChunkMeta meta,
                             String path) throws IOException, InterruptedException {
        try (RandomAccessFile outputFile = new RandomAccessFile(path, "rw")) {
            int read;
            byte[] buffer = new byte[16 * 1024];

            long cursor = meta.getStartPosition();
            long byteCounter = 0;

            log.info("Start writing...");
            long loopCounter = 0;
            while ((read = inputStream.read(buffer)) > -1) {

                byteCounter += read;
                validateSize(meta, byteCounter);

                outputFile.seek(cursor);
                outputFile.write(buffer, 0, read);
                cursor += read;

                if (++loopCounter % 10_000 == 0) {
                    val message = String.format("Downloading&writing... %.0f%%", byteCounter * 100.0 / meta.getSize());
                    log.info(message);
                }
            }
        }
        log.info("Complete writing into file: Chunk id=" + meta.getId());
    }

    private void validateSize(final VideoChunkMeta meta, long byteCounter) {

        if (byteCounter > meta.getSize()) {
            val msg = String.format("Actual file size is greater than expected! Video chunk id = %s", meta.getId());
            throw new FileSizeExceedsMetaInfoException(msg);
        }
    }
}
