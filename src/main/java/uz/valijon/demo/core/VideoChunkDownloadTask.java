package uz.valijon.demo.core;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.concurrent.Callable;
import java.util.function.Consumer;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import uz.valijon.demo.checksum.ChecksumValidator;
import uz.valijon.demo.models.DownloadResult;
import uz.valijon.demo.models.VideoChunkMeta;

@RequiredArgsConstructor
@Slf4j
public class VideoChunkDownloadTask implements Callable<DownloadResult> {

    private VideoChunkMeta chunkMeta;
    private String path;

    private final VideoChunkDownloader downloader;
    private final VideoChunkWriter writer;

    @Override
    @SneakyThrows
    public DownloadResult call() throws Exception {
        validateState();
        val startTime = System.currentTimeMillis();
        Thread.currentThread().setName("Thread_Chunk_" + chunkMeta.getOrderNumber());
        log.info("Start thread! File chunk id=" + chunkMeta.getId());

        try {

            downloader.download(chunkMeta.getId(), workWhileDownloading());

            return DownloadResult.success(startTime, chunkMeta);
        } catch (Exception e) {

            log.debug("Error while downloading/writing video chunk!", e);
            return DownloadResult.error(e.getMessage(), startTime, chunkMeta);
        }
    }

    private Consumer<InputStream> workWhileDownloading() {
        return new Consumer<InputStream>() {

            @SneakyThrows
            public void accept(InputStream is) {
                val validator = new ChecksumValidator(is, chunkMeta);
                is = validator.getDigestInputStream();
                is = new BufferedInputStream(is);

                writer.writeContent(is, chunkMeta, path);

                log.info("Complete downloading. Chunk id=" + chunkMeta.getId());

                validator.validateHash();
            }
        };
    }

    /**
     * This should be invoked before starting the thread
     */
    public void init(final VideoChunkMeta chunkMeta,
                     final String path) {
        beforeInitValidation();

        this.chunkMeta = chunkMeta;
        this.path = path;
    }

    private void beforeInitValidation() {
        val notFirstTime = this.chunkMeta != null || path != null;
        if (notFirstTime) {
            log.debug("DEVELOPMENT ERROR!");
            throw new IllegalStateException("Trying to invoke init second time!");
        }
    }

    private void validateState() {
        if (this.chunkMeta == null) {
            log.debug("DEVELOPMENT ERROR!");
            throw new IllegalStateException("Video chunk meta is not provided!");
        }

        if (this.path == null) {
            log.debug("DEVELOPMENT ERROR!");
            throw new IllegalStateException("Output file path is not provided!");
        }
    }
}
