package uz.valijon.demo.core;

import java.util.List;
import java.util.concurrent.Future;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import uz.valijon.demo.checksum.Validator;
import uz.valijon.demo.models.DownloadResult;

@Slf4j
public class ProcessMonitoringTask implements Runnable {

    private Validator validator;
    private List<Future<DownloadResult>> tasks;

    public ProcessMonitoringTask(final List<Future<DownloadResult>> tasks, Validator validator) {
        this.tasks = tasks;
        this.validator = validator;
    }

    @SneakyThrows
    @Override
    public void run() {
        log.info("Start running");
        while (true) {
            monitor();
            if (tasks.isEmpty()) {
                validator.validate();
                break;
            }
            Thread.sleep(1000);
        }
    }

    @SneakyThrows
    private void monitor() {
        val taskIterator = tasks.iterator();
        while (taskIterator.hasNext()) {
            val current = taskIterator.next();
            if (current.isDone()) {
                current.get().printStatus();
                taskIterator.remove();
            }
        }
    }
}
