package uz.valijon.demo.core;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;
import java.util.function.Consumer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestTemplate;
import uz.valijon.demo.utils.Utils;

@Service
@RequiredArgsConstructor
@Slf4j
public class VideoChunkDownloader {

    private final RestTemplate template;

    public void download(final UUID chunkId, Consumer<InputStream> consumer) throws IOException {
        val path = "/movies/" + chunkId;
        val server = Utils.decideServer(path);
        val url = "https://" + server.getHost() + path;

        log.info("Start downloading... Chunk id = " + chunkId);

        final ResponseExtractor<InputStream> inputStreamResponseExtractor =
                (ClientHttpResponse clientHttpResponse) -> {
                    consumer.accept(clientHttpResponse.getBody());
                    return null;
                };

        template.execute(url, HttpMethod.GET, null, inputStreamResponseExtractor);
    }
}
