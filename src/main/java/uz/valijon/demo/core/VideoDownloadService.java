package uz.valijon.demo.core;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import uz.valijon.demo.checksum.ChecksumValidator;
import uz.valijon.demo.models.DownloadResult;
import uz.valijon.demo.models.VideoMeta;

@Service
@RequiredArgsConstructor
public class VideoDownloadService {

    @Value("${local.downloads.folder}")
    private String downloadsFolder;

    private final VideoChunkDownloader downloader;
    private final VideoChunkWriter chunkWriter;

    public void asyncDownload(final VideoMeta videoMeta) throws FileNotFoundException {
        val outputFilePath = downloadsFolder + videoMeta.getFileName();
        videoMeta.setOutputFilePath(outputFilePath);
        videoMeta.validate();
        videoMeta.calculateChunkPositions();

        val poolSize = videoMeta.getNumberOfChunks() + 1;
        val executorService = Executors.newFixedThreadPool(poolSize);
        val futures = new ArrayList<Future<DownloadResult>>();
        for (val task : tasks(videoMeta)) {
            val future = executorService.submit(task);
            futures.add(future);
        }

        val fullFileValidator = ChecksumValidator.fullFileValidator(videoMeta);
        executorService.execute(new ProcessMonitoringTask(futures, fullFileValidator));
    }

    private Set<VideoChunkDownloadTask> tasks(final VideoMeta videoMeta) {
        Set<VideoChunkDownloadTask> tasks = new HashSet<>();
        videoMeta.getChunks()
                 .forEach(chunkMeta -> {
                     val videoChunkDownloadTask = makeNewTask();
                     videoChunkDownloadTask.init(chunkMeta, videoMeta.getOutputFilePath());
                     tasks.add(videoChunkDownloadTask);
                 });

        return tasks;
    }

    private VideoChunkDownloadTask makeNewTask() {
        return new VideoChunkDownloadTask(downloader, chunkWriter);
    }
}
