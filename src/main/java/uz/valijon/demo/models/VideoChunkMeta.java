package uz.valijon.demo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class VideoChunkMeta {

    private UUID id;
    private String hash;
    private Long size;
    @JsonProperty("order_number")
    private Integer orderNumber;
    /**
     * Index of the starting byte in merged file
     */
    @JsonIgnore
    private Long startPosition;
}
