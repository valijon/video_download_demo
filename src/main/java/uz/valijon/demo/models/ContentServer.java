package uz.valijon.demo.models;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

@Data
@Builder
@ToString
@Slf4j
public class ContentServer {

    public ContentServer() { }

    public ContentServer(final String name, final String host) {
        this.name = name;
        this.host = host;
    }

    private String name;
    private String host;

    public ContentStatus checkStatus(RestTemplate restClient, String contentPath) {
        val url = "https://" + host + contentPath;
        var isAvailable = false;
        var latency = Integer.MAX_VALUE;

        try {
            val startTime = System.currentTimeMillis();
            val response = sendHeadRequest(restClient, url);
            isAvailable = response.getStatusCode() == HttpStatus.OK;
            latency = (int) (System.currentTimeMillis() - startTime);
        } catch (HttpClientErrorException | ResourceAccessException e) {
            log.debug("Server is not available!", e);
        }

        return ContentStatus.builder()
                            .isAvailable(isAvailable)
                            .latency(latency)
                            .build();
    }

    private ResponseEntity<Void> sendHeadRequest(final RestTemplate restClient,
                                                 final String url) {
        return restClient.exchange(url, HttpMethod.HEAD, HttpEntity.EMPTY, Void.class);
    }
}
