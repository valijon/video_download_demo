package uz.valijon.demo.models;

import static uz.valijon.demo.utils.Utils.factorial;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import lombok.val;

@Data
@Builder
@ToString
public class VideoMeta {

    private UUID id;
    @JsonProperty("file_name")
    private String fileName;
    private String hash;
    private Long size;
    @JsonProperty("number_of_chunks")
    private Integer numberOfChunks;
    private List<VideoChunkMeta> chunks;
    @JsonIgnore
    private String outputFilePath;

    public void calculateChunkPositions() {
        long index = 0;
        for (val meta : chunks) {
            meta.setStartPosition(index);
            index += meta.getSize();
        }
    }

    public void validate() {
        if (hash == null || hash.isBlank()) {
            throw new IllegalArgumentException("Incorrect hash for file!");
        }

        if (fileName == null || fileName.isBlank()) {
            throw new IllegalArgumentException("Incorrect file name!");
        }

        if (outputFilePath == null) {
            throw new IllegalStateException("Output file path is not set!");
        }

        validateChunks();
    }

    private void validateChunks() {
        try {
            if (chunks == null || chunks.size() != numberOfChunks) {
                throw new IllegalArgumentException("Please check number of chunks!");
            }

            val actual = chunks.stream()
                               .mapToInt(VideoChunkMeta::getOrderNumber)
                               .reduce((n, m) -> n * m)
                               .getAsInt();

            val expected = factorial(numberOfChunks);

            if (actual != expected) {
                throw new IllegalArgumentException("Chunks contain wrong order number!");
            }

            val chunksSize = chunks.stream()
                                   .mapToLong(VideoChunkMeta::getSize)
                                   .sum();

            if (this.size != chunksSize) {
                throw new IllegalArgumentException("Chunks` sizes are incorrect!");
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            throw new IllegalArgumentException("Illegal meta for file chunks");
        }
    }
}
