package uz.valijon.demo.models;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode
public class ContentStatus implements Comparable<ContentStatus> {

    private boolean isAvailable;
    private int latency;

    @Override
    public int compareTo(final ContentStatus other) {
        if (this.isAvailable && other.isAvailable) {

            return other.latency - this.latency;
        } else if (this.isAvailable && !other.isAvailable) {

            return 1;
        } else if (!this.isAvailable && other.isAvailable) {

            return -1;
        } else {

            return 0;
        }
    }
}