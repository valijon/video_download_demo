package uz.valijon.demo.models;

import java.io.RandomAccessFile;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Builder
@Slf4j
public class DownloadResult {

    private boolean isOk;
    private int timeSpent;
    private String cause;
    private VideoChunkMeta meta;
    private RandomAccessFile outputFile;

    public static DownloadResult success(final long startTime,
                                         final VideoChunkMeta meta) {
        return DownloadResult.builder()
                             .isOk(true)
                             .meta(meta)
                             .timeSpent((int) (System.currentTimeMillis() - startTime))
                             .build();
    }

    public static DownloadResult error(final String message,
                                       final long startTime,
                                       final VideoChunkMeta meta) {
        return DownloadResult.builder()
                             .isOk(false)
                             .meta(meta)
                             .timeSpent((int) (System.currentTimeMillis() - startTime))
                             .cause(message)
                             .build();
    }

    public void printStatus() {
        log.info("*********************************");
        log.info("Chunk id = " + meta.getId());
        log.info("Chunk size = " + meta.getId());
        if (isOk) {
            log.info("Status: success");
        } else {
            log.info("Status: fail! Cause: " + cause);
        }
        log.info("*********************************");
    }
}
