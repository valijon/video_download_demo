package uz.valijon.demo.utils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import org.springframework.web.client.RestTemplate;
import uz.valijon.demo.config.ContentServerConfig;
import uz.valijon.demo.models.ContentServer;
import uz.valijon.demo.models.ContentStatus;

public class Utils {

    public static final List<ContentServer> contentServers = new ArrayList<>();

    private static RestTemplate restTemplate;

    public static long factorial(int n) {
        if (n <= 2) {
            return n;
        }
        return n * factorial(n - 1);
    }

    public static ContentServer decideServer(final String pathToResource) {
        Pair<ContentServer, ContentStatus> nearestServer =
                contentServers.stream()
                              .map(server -> new Pair<>(
                                      server,
                                      server.checkStatus(restTemplate, pathToResource)
                              ))
                              .max(Comparator.comparing(Pair::getValue))
                              .orElseThrow(() -> new IllegalStateException(
                                      "No server is available"));

        if (!nearestServer.getValue().isAvailable()) {
            throw new IllegalStateException("No server is available");
        }

        return nearestServer.getKey();
    }

    public static void init(final RestTemplate restTemplate, final ContentServerConfig config) {
        Utils.restTemplate = restTemplate;
        contentServers.addAll(config.getServers());
    }
}

