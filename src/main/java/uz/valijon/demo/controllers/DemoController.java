package uz.valijon.demo.controllers;

import java.io.FileNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import uz.valijon.demo.models.VideoMeta;
import uz.valijon.demo.core.VideoDownloadService;

@RestController
@RequiredArgsConstructor
public class DemoController {

    private final VideoDownloadService videoDownloadService;

    @PostMapping("/download/video")
    public void downloadVideo(@RequestBody VideoMeta videoMeta) throws FileNotFoundException {
        videoDownloadService.asyncDownload(videoMeta);
    }

}
