package uz.valijon.demo.checksum;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.codec.binary.Hex;
import org.springframework.util.StreamUtils;
import uz.valijon.demo.exception.HashMismatchException;
import uz.valijon.demo.models.VideoChunkMeta;
import uz.valijon.demo.models.VideoMeta;

@Slf4j
public class ChecksumValidator implements AutoCloseable {

    private final MessageDigest md;
    private final DigestInputStream digestInputStream;
    private final VideoChunkMeta chunkMeta;

    @SneakyThrows
    public ChecksumValidator(final InputStream inputStream, final VideoChunkMeta chunkMeta) {
        this.md = MessageDigest.getInstance("MD5");
        this.digestInputStream = new DigestInputStream(inputStream, md);
        this.chunkMeta = chunkMeta;
    }

    @Override
    public void close() throws Exception {
        this.digestInputStream.close();
    }

    public void validateHash() {
        byte[] digest = md.digest();
        val actualHash = Hex.encodeHexString(digest).toUpperCase();

        if (!chunkMeta.getHash().equals(actualHash)) {
            val message = "Checksum didn't match! File chunk id = " + chunkMeta.getId();
            log.error(message);
            throw new HashMismatchException(message);
        }
    }

    public DigestInputStream getDigestInputStream() {
        return digestInputStream;
    }

    public static Validator fullFileValidator(final VideoMeta videoMeta) {
        val outputFilePath = videoMeta.getOutputFilePath();
        val expectedHash = videoMeta.getHash();

        return () -> {

            final MessageDigest md = MessageDigest.getInstance("MD5");
            try (val fis = new FileInputStream(new File(outputFilePath));
                 val dis = new DigestInputStream(fis, md)

            ) {

                //we need to read stream, but content is not needed
                StreamUtils.drain(dis);

                byte[] digest = md.digest();
                val actualHash = Hex.encodeHexString(digest).toUpperCase();

                if (!expectedHash.equals(actualHash)) {
                    val message = "Checksum didn't match for output file! ";
                    log.error(message);

                    throw new HashMismatchException(message);
                } else {
                    log.info("Success hash validation for output file!");
                    log.info(String.format("Expected hash = %s, actual hash = %s", expectedHash, actualHash));
                }
            }
        };
    }
}
