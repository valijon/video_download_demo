package uz.valijon.demo.checksum;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public interface Validator {

    void validate() throws IOException, NoSuchAlgorithmException;
}
