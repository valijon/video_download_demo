package uz.valijon.demo.checksum;

import java.security.MessageDigest;
import lombok.SneakyThrows;
import org.apache.commons.codec.binary.Hex;

public class ChecksumCalculator {

    private MessageDigest messageDigest;

    @SneakyThrows
    public ChecksumCalculator() {
        this.messageDigest = MessageDigest.getInstance("MD5");
    }

    public void update(byte[] buffer, int read) {
        messageDigest.update(buffer, 0, read);
    }

    public String calcChecksum() {
        byte[] digest = messageDigest.digest();

        return Hex.encodeHexString(digest).toUpperCase();
    }
}
