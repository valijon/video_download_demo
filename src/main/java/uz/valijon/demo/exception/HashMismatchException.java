package uz.valijon.demo.exception;

public class HashMismatchException extends RuntimeException {

    public HashMismatchException(final String message) {
        super(message);
    }
}
