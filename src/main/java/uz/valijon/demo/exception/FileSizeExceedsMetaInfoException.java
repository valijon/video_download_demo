package uz.valijon.demo.exception;

public class FileSizeExceedsMetaInfoException extends RuntimeException {

    public FileSizeExceedsMetaInfoException(final String message) {
        super(message);
    }
}
