package uz.valijon.demo.exception;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Map<String, String>> handleWrongArg(final Exception ex) {
        log.error("Wrong argument passed", ex);
        val body = Map.of("message", ex.getMessage());

        return ResponseEntity.status(BAD_REQUEST).body(body);
    }

    @ExceptionHandler(IllegalStateException.class)
    public ResponseEntity<Map<String, String>> handleIllegalState(final Exception ex) {
        log.error("Incorrect settings!", ex);
        val body = Map.of("message", ex.getMessage());

        return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(body);
    }
}