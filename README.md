# README #

We need to download large file from remote servers by file chunks. For example 20GB video may have 20 chunks each 1GB.

### Issues to solve ###

* SSL/TLS client authentication
* Decide content server to download from (file should be availabe & the location is nearest) 
* Limited RAM (Do not cache InputStream)
* Concurrently download all chunks
* Verify checksum for each chunk on the fly (No additional processing)
* Merge chunks on the fly (Merge file chunks as you download)
* Validate checksum of final output

###Short description of the implementation

#### SSL/TLS client authentication
Enabled client authentication on nginx config;
Created CRS with user's private key
Singed it with CA certificate and private key and converted the output to jks file.
Use Jks from standard RestTemplate.

#### Decide content server
Send HEAD request for each file chunk to be downloaded.
(Previously offered solution: downloading small file from remote servers are not good because it doesn't solve the following: 1. May be
 slow 2. May be 404
 for
 exact file we need ) 

#### Limited RAM
Access response(from nginx) body directly(to make sure it is NOT converted to ByteArrayInputStream)

#### Concurrently download all chunks
Created Callable for each download task and submitted them to ExecutorService

#### Verify checksum for each chunk on the fly
Implemented two solutions: 1. custom ChecksumCalculator class; 2.DigestInputStream

#### Merge chunks on the fly
Created RandomAccessFile object and updated it's content from multiple threads in a synchronized block.

#### Validate checksum of final output
When all other threads finished I got InputStream from output file and passed it to DigestInputStream...
It is not possible to validate it on the fly since order of bytes matter and they are too large to cache them.  